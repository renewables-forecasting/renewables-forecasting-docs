Welcome to renewables-forecasting's documentation!
=========================================

.. toctree::
   :maxdepth: 3
   :caption: Contents
   :hidden:

   literature/index
   method
   data_used/index
   glossary
   references

.. include:: readme.rst
